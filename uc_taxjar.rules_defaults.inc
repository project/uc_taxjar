<?php

/**
 * @file
 * Default Rules for uc_taxjar.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function uc_taxjar_default_rules_configuration() {
  $rules = array();
  $rules['uc_taxjar_save_order'] = rules_import(
    '{ "uc_taxjar_save_order" : {
        "LABEL" : "Save order to TaxJar",
        "PLUGIN" : "reaction rule",
        "OWNER" : "rules",
        "REQUIRES" : [ "rules", "uc_taxjar", "uc_order" ],
        "ON" : { "uc_order_status_update" : [] },
        "IF" : [
          { "data_is" : {
              "data" : [ "updated-order:order-status" ],
              "value" : "payment_received"
            }
          }
        ],
        "DO" : [ { "uc_taxjar_order_save" : { "uc_order" : [ "updated-order" ] } } ]
      }
    }'
  );
  $rules['uc_taxjar_delete_order'] = rules_import(
    '{ "uc_taxjar_delete_order" : {
        "LABEL" : "Delete order from TaxJar",
        "PLUGIN" : "reaction rule",
        "OWNER" : "rules",
        "REQUIRES" : [ "uc_taxjar", "uc_order" ],
        "ON" : { "uc_order_delete" : [] },
        "DO" : [ { "uc_taxjar_order_delete" : { "uc_order" : [ "order" ] } } ]
      }
    }'
  );
  $rules['refund_order_in_taxjar'] = rules_import(
    '{ "refund_order_in_taxjar" : {
        "LABEL" : "Refund order in TaxJar",
        "PLUGIN" : "reaction rule",
        "OWNER" : "rules",
        "REQUIRES" : [ "rules", "uc_taxjar", "uc_order" ],
        "ON" : { "uc_order_status_update" : [] },
        "IF" : [
          { "NOT data_is" : { "data" : [ "order:order-status" ], "value" : "canceled" } },
          { "data_is" : { "data" : [ "updated-order:order-status" ], "value" : "canceled" } }
        ],
        "DO" : [ { "uc_taxjar_order_refund" : { "uc_order" : [ "updated-order" ] } } ]
      }
    }'
  );
  $rules['uc_taxjar_update_order'] = rules_import(
    '{ "uc_taxjar_update_order" : {
        "LABEL" : "Update order in TaxJar",
        "PLUGIN" : "reaction rule",
        "OWNER" : "rules",
        "REQUIRES" : [ "rules", "uc_taxjar", "uc_order" ],
        "ON" : { "uc_order_status_update" : [] },
        "IF" : [
          { "data_is" : {
              "data" : [ "order:order-status" ],
              "op" : "IN",
              "value" : { "value" : {
                  "canceled" : "canceled",
                  "pending" : "pending",
                  "processing" : "processing",
                  "payment_received" : "payment_received",
                  "completed" : "completed"
                }
              }
            }
          }
        ],
        "DO" : [ { "uc_taxjar_order_update" : { "uc_order" : [ "updated-order" ] } } ]
      }
    }'
  );
  return $rules;
}
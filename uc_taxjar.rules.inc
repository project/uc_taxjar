<?php

/**
 * @file
 * Rules supporting TaxJar integration.
 */

/**
 * Implements hook_rules_action_info().
 */
function uc_taxjar_rules_action_info() {
  return array(
    'uc_taxjar_order_save' => array(
      'label' => t('Save an order transaction to TaxJar'),
      'group' => t('Ubercart TaxJar'),
      'parameter' => array(
        'uc_order' => array(
          'type' => 'uc_order',
          'label' => t('Ubercart Order'),
        ),
      ),
    ),
    'uc_taxjar_order_update' => array(
      'label' => t('Update an order transaction in TaxJar'),
      'group' => t('Ubercart TaxJar'),
      'parameter' => array(
        'uc_order' => array(
          'type' => 'uc_order',
          'label' => t('Ubercart Order'),
        ),
      ),
    ),
    'uc_taxjar_order_refund' => array(
      'label' => t('Refund an order transaction in TaxJar'),
      'group' => t('Ubercart TaxJar'),
      'parameter' => array(
        'uc_order' => array(
          'type' => 'uc_order',
          'label' => t('Ubercart Order'),
        ),
      ),
    ),
    'uc_taxjar_order_delete' => array(
      'label' => t('Delete an order transaction in TaxJar'),
      'group' => t('Ubercart TaxJar'),
      'parameter' => array(
        'uc_order' => array(
          'type' => 'uc_order',
          'label' => t('Ubercart Order'),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_rules_condition_info().
 */
function uc_taxjar_rules_condition_info() {
  return array(
    'uc_order_contains_taxjar_line_item' => array(
      'label' => t('Order contains a tax line item'),
      'parameter' => array(
        'uc_order' => array(
          'type' => 'uc_order',
          'label' => t('Ubercart Order'),
          'wrapped' => TRUE,
        ),
      ),
      'group' => t('Ubercart TaxJar'),
      'callbacks' => array(
        'execute' => 'uc_taxjar_rules_contains_tax_line',
      ),
    ),
  );
}

/**
 * Check whether an order has a tax line item.
 *
 * @param $order
 *   The order entity.
 *
 * @return bool
 *   Whether a tax line item exists.
 */
function uc_taxjar_rules_contains_tax_line($order) {
  foreach($order->line_items as $line_item) {
    if ($line_item['type'] == 'tax' && !empty($line_item['data']['tax_id'])) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Provides options for exclusion mode.
 */
function uc_taxjar_mode_options_list() {
  return array('exclusive' => 'Order contains ONLY products of the selected category', 'inclusive' => 'Order contains ONE OR MORE products of the selected category');
}

/**
 * Provides options for product categories.
 */
function uc_taxjar_category_options_list() {
  $options = array();
  $vocabulary = taxonomy_vocabulary_machine_name_load('taxjar_categories');
  $terms = taxonomy_get_tree($vocabulary->vid);
  foreach ($terms as $term) {
    $options[$term->tid] = $term->name;
  }
  return $options;
}

/**
 * Provides options for address profiles.
 */
function uc_taxjar_profile_options_list() {
  $options = array();
  $profiles = commerce_customer_profile_types();
  foreach ($profiles as $key => $type) {
    $options[$key] = $type['name'];
  }
  return $options;
}

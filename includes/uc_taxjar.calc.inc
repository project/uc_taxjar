<?php

/**
 * @file
 * Handles request to Taxjar for sales tax values.
 */

/**
 * @file
 * TaxJar calculation/requests functions.
 */

/**
 * Prepares the tax request array to be sent to TaxJar.
 *
 * @param object $order
 *   The current order object
 *
 * @return array|bool
 *   Returns the request body array to be sent, FALSE in case of failure.
 */
function uc_taxjar_create_tax_request($order) {
  // Prepare the Request Body.
  $request_body = _uc_taxjar_prepare_request_body($order);

  if (empty($request_body)) {
    return FALSE;
  }

  // Add plugin identifier to request.
  $request_body['plugin'] = 'drupal-ubercart';
  
  _uc_taxjar_request_add_lines($request_body, $order);
  drupal_alter('uc_taxjar_tax_request', $request_body, $order);
  return $request_body;
}

/**
 * Prepares the request body.
 *
 * @param object $order
 *   The current order object.
 */
function _uc_taxjar_prepare_request_body($order) {
  $request_body = array();

  // Get delivery info out.
  $tax_address_profile = variable_get('uc_taxjar_tax_address', '');
  if ($tax_address_profile == 'Billing') {
    $request_body['to_street'] = $order->billing_street1;
    if (!empty($order->billing_street2)) {
      $request_body['to_street'] .= ' ' . $order->billing_street2;
    }
    $request_body['to_city'] = $order->billing_city;
    $request_body['to_state'] = uc_get_zone_code($order->billing_zone);
    $request_body['to_zip'] = $order->billing_postal_code;
    $country_code = $order->billing_country;
  }
  elseif ($tax_address_profile == 'Shipping') {
    $request_body['to_street'] = $order->delivery_street1;
    if (!empty($order->delivery_street2)) {
      $request_body['to_street'] .= ' ' . $order->delivery_street2;
    }
    $request_body['to_city'] = $order->delivery_city;
    $request_body['to_state'] = uc_get_zone_code($order->delivery_zone);
    $request_body['to_zip'] = $order->delivery_postal_code;
    $country_code = $order->delivery_country;
  }

  // Convert Country Code to Country Abbreviation.
  $country_data = uc_get_country_data(array('country_id' => $country_code));
  $request_body['to_country'] = $country_data[0]['country_iso_code_2'];

  // Get User UID.
  $my_user = $order->uid ? user_load($order->uid) : $user;
  if (!isset($my_user->name)) {
    $user_id = uc_store_email_to_username($order->primary_email);
  }
  else {
    $user_id = $my_user->name;
  }

  // Include total order amount
  $request_body['amount'] = $order->order_total;

  // Add from address if configured to use module addresses.
  if (variable_get('uc_taxjar_shipping_mode', 'on_file') == 'primary') {
    $request_body['from_country'] = variable_get('uc_taxjar_primary_country', 'US');
    $request_body['from_zip'] = variable_get('uc_taxjar_primary_zip', '');
    $request_body['from_state'] = variable_get('uc_taxjar_primary_state', '');
    $request_body['from_city'] = variable_get('uc_taxjar_primary_city', '');
    $request_body['from_street'] = variable_get('uc_taxjar_primary_street', '');
  }

  return $request_body;
}

/**
 * Prepares the transaction array to be sent to TaxJar.
 *
 * @param $order
 *   The order entity.
 *
 * @return array|bool
 *   Returns the request body array to be sent, FALSE in case of failure.
 */
function uc_taxjar_create_transaction_request($order) {
  // Return FALSE in case there are no line items.
  if (count($order->products) === 0) {
    return FALSE;
  }

  // Prepare the Request Body.
  $request_body = _uc_taxjar_prepare_request_body($order);

  if (empty($request_body)) {
    return FALSE;
  }

  if (!_uc_taxjar_request_add_lines($request_body, $order, TRUE)) {
    return FALSE;
  }

  $request_body['transaction_id'] = $order->order_id;
  $request_body['transaction_date'] = format_date($order->created, 'custom', 'c');
  $request_body['amount'] = $order->order_total - $request_body['sales_tax'];

  drupal_alter('uc_taxjar_transaction_request', $request_body, $order);

  return $request_body;
}

/**
 * Returns the transaction line items that will be sent to the API.
 *
 * @param array $request_body
 *   The request body that needs to be altered.
 * @param $line_items
 *   An array of line items wrapper that need to be added to the transaction.
 * @param bool $is_transaction
 *   A flag which indicates whether we are building for a
 *   tax request or transaction request.
 */
function _uc_taxjar_request_add_lines(array &$request_body, $order, $is_transaction = FALSE) {
  $lines = array();
  $shipping = 0;

  if ($is_transaction) {
    $request_body['sales_tax'] = 0;
    foreach ($order->line_items as $line_item) {
      if ($line_item['type'] != 'tax') {
        continue;
      }
      $request_body['sales_tax'] = $line_item['amount'];
      break;
    }
    if (!isset($request_body['sales_tax'])) {
      return FALSE;
    }
  }

  // Loop over the product line items in the order.
  foreach ($order->products as $prod) {
    $tax_code = '';
    if (isset($prod->uc_taxjar_tax_code[LANGUAGE_NONE][0]['tid'])) {
      $taxjar_array = $prod->uc_taxjar_tax_code;
      if ($tid = $taxjar_array[LANGUAGE_NONE][0]['tid']) {
        $taxonomy_term = taxonomy_term_load($tid);
        $tax_code = entity_metadata_wrapper('taxonomy_term', $taxonomy_term)->taxjar_category_code->value();
      }
    }
    else {
      $product = node_load($prod->nid);
      if (isset($product->uc_taxjar_tax_code[LANGUAGE_NONE][0]['tid'])) {
        if ($tid = $product->uc_taxjar_tax_code[LANGUAGE_NONE][0]['tid']) {
          $taxonomy_term = taxonomy_term_load($tid);
          $newline['product_tax_code'] = entity_metadata_wrapper('taxonomy_term', $taxonomy_term)->taxjar_category_code->value();
        }
      }
    }
    $newline = array(
      'id' => $prod->order_product_id,
      'quantity' => $prod->qty,
      'product_identifier' => $prod->model,
      'description' => $prod->title,
      'unit_price' => $prod->price,
      'product_tax_code' => $tax_code,
    );
    $lines[] = $newline;
  }
  
  foreach ($order->line_items as $key => $item) {
    if (in_array($item['type'], array('coupon'))) {
      $newline = array(
        'id' => $item['line_item_id'],
        'quantity' => 1,
        'product_identifier' => $item['type'],
        'description' => $item['title'],
        'unit_price' => $item['amount'],
        'product_tax_code' => $tax_code,
      );
      $lines[] = $newline;
    }
    elseif (in_array($item['type'], array('shipping'))) {
      $request_body['shipping'] = $item['amount'];      
    }
  }
  if ($lines) {
    $request_body['line_items'] = $lines;
  }
  return TRUE;
}

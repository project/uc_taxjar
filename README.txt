This module integrates the TaxJar service with Ubercart.
The TaxJar platform provides automated sales tax calculation, reporting, and filing for your business.

##Features
Accurately calculate and collect sales tax in Ubercart.

Automatically track and file your sales tax collections through the TaxJar platform, with breakdowns by state, county, and local jurisdictions.

Simple to setup, be up and running in under 5 minutes.

Have a custom ubercart checkout process? Ubercart TaxJar integrates with Rules for ultimate flexibility and can be easily customized to fit any custom Ubercart configuration.

##Requirements
The Ubercart TaxJar module requires Ubercart to be installed.